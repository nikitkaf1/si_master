﻿using Newtonsoft.Json;
using Plugin.Settings;
using SI_Master.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SI_Master.Settings
{
    public static class AuthSettings
    {
        public static int Active
        {
            get
            {
                if (CrossSettings.Current.Contains("ActiveUser"))
                {
                    return CrossSettings.Current.GetValueOrDefault("ActiveUser", -1);
                }
                return -1;
            }
            set
            {
                CrossSettings.Current.AddOrUpdateValue("ActiveUser", value);
            }
        }

        public static bool Remove(string login)
        {
            List<UserAuthData> users = Read();
            int idx = users.FindIndex(u => { return string.Equals(u.Login, login); });
            if (idx != -1)
            {
                users.RemoveAt(idx);
                if (Active == idx) Active = -1;
                Write(users);
                return true;
            }
            return false;
        }

        public static int AddOrUpdate(UserAuthData user)
        {
            List<UserAuthData> users = Read();
            int idx = users.FindIndex(u => { return string.Equals(u.Login, user.Login); });
            if (idx != -1)
            {
                users.RemoveAt(idx);
                users.Insert(idx, user);
            }
            else
            {
                idx = users.Count;
                users.Add(user);
            }
            Write(users);
            return idx;
        }

        public static void Write(List<UserAuthData> users)
        {
            string json = JsonConvert.SerializeObject(users, new JsonSerializerSettings() { StringEscapeHandling = StringEscapeHandling.EscapeNonAscii });
            CrossSettings.Current.AddOrUpdateValue("Users", json);
        }

        public static List<UserAuthData> Read()
        {
            //if (CrossSettings.Current.Contains("Users"))
            //{
            //    try
            //    {
            //        return JsonConvert.DeserializeObject<List<UserAuthData>>(CrossSettings.Current.GetValueOrDefault("Users", ""),
            //            new JsonSerializerSettings() { StringEscapeHandling = StringEscapeHandling.EscapeNonAscii });
            //    }
            //    catch
            //    { }
            //}
            //захардкодил зареганного пользователя пользователя
            List<UserAuthData> userrsList = new List<UserAuthData>();
            UserAuthData currentUser = new UserAuthData()
            {
                Caption = "Ц0000820",
            Login = "329BD8",
            Password = "w+XZZ0xkFXGo3ojUJ2DciPxeWMuX45mPe9s7vV9tZHM="
        };
            userrsList.Add(currentUser);
            return userrsList;
        }
    }
}
