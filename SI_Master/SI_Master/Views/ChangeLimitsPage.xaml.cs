﻿using SI_Master.Models;
using SI_Master.ViewModels.ChangeLimitsPage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SI_Master.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChangeLimitsPage : ContentPage
    {

        IChangeLimitsPageViewModel viewModel = DependencyService.Get<IChangeLimitsPageViewModel>();
        public ChangeLimitsPage(DictionaryLimits dictionaryLimits)
        {
            InitializeComponent();
            viewModel.SetDictionary(dictionaryLimits);
            BindingContext = viewModel;
        }
    }
}