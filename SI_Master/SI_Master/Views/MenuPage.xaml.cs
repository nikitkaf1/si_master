﻿using SI_Master.Common;
using SI_Master.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SI_Master.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MenuPage : ContentPage
    {
        MainPage RootPage { get => Application.Current.MainPage as MainPage; }
        List<HomeMenuItem> menuItems;
        public MenuPage()
        {
            InitializeComponent();

            menuItems = new List<HomeMenuItem>
            {
                new HomeMenuItem {Id = MenuItemType.DeskTop, Title="Рабочий стол" },
                new HomeMenuItem {Id = MenuItemType.Cards, Title="Топливные карты" },
                new HomeMenuItem {Id = MenuItemType.Transactions, Title="Транзакции" },
                new HomeMenuItem {Id = MenuItemType.Settlements, Title="Взаиморасчёты" },
                new HomeMenuItem {Id = MenuItemType.ArchiveCardLimits, Title="Заявки на лимиты" },
                new HomeMenuItem {Id = MenuItemType.ArchiveCardOrder, Title="Заявки на карты" },
                new HomeMenuItem {Id = MenuItemType.ArchiveCardBlock, Title="Заявки на блокировку" },
                new HomeMenuItem {Id = MenuItemType.Dashboard, Title="Карточка предприятия" },
                new HomeMenuItem {Id = MenuItemType.ChangeUser, Title="Смена пользователя" },
                new HomeMenuItem {Id = MenuItemType.Exit, Title="Выход" }
            };

            switch (Device.RuntimePlatform)
            {
                case Device.iOS:
                    userInfo.Padding = new Thickness(10, 8 + DependencyService.Get<IStatusBar>().GetHeight(), 10, 20);
                    break;

                default:
                    userInfo.Padding = new Thickness(10, 8, 10, 20);
                    break;
            }
            ListViewMenu.ItemsSource = menuItems;

            ListViewMenu.SelectedItem = menuItems[0];
            ListViewMenu.ItemSelected += async (sender, e) =>
            {
                if (e.SelectedItem == null)
                    return;

                var id = (int)((HomeMenuItem)e.SelectedItem).Id;
                await RootPage.NavigateFromMenu(id);
            };
        }

        private void UserInfo_Tapped(object sender, EventArgs e)
        {
            //TODO
        }
    }
}