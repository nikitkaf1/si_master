﻿using Acr.UserDialogs;
using SI_Master.ViewModels.TransactionsPage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SI_Master.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TransactionsPage : ContentPage
    {

        ITransactionsPageViewModel viewModel = DependencyService.Get<ITransactionsPageViewModel>();
        public TransactionsPage()
        {
            InitializeComponent();
            BindingContext = viewModel;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            UserDialogs.Instance.ShowLoading();
            await viewModel.LoadTransactions();
            UserDialogs.Instance.HideLoading();
        }
    }
}