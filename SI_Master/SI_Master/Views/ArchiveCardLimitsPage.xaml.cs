﻿using Acr.UserDialogs;
using Rg.Plugins.Popup.Services;
using SI_Master.Models;
using SI_Master.PopUps;
using SI_Master.ViewModels.ArchiveCardsLimitsPage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SI_Master.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ArchiveCardLimitsPage : ContentPage
    {
        IArchiveCardsLimitsPageViewModel viewModel = DependencyService.Get<IArchiveCardsLimitsPageViewModel>();
        public ArchiveCardLimitsPage()
        {
            InitializeComponent();
            BindingContext = viewModel;
            ArchiveCardsListView.ItemSelected += (object sender, SelectedItemChangedEventArgs e) =>
            {
                var limits = (ArchiveCardLimits)e.SelectedItem;
                var cardLimitsPopup = new CardLimitsPopup(limits.Limits);
                PopupNavigation.Instance.PushAsync(cardLimitsPopup);
            };
        }



        protected override async void OnAppearing()
        {
            base.OnAppearing();
            UserDialogs.Instance.ShowLoading();
            await viewModel.LoadArchiveCardLimits();
            UserDialogs.Instance.HideLoading();
        }
    }
}