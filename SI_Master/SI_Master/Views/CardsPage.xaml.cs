﻿using Acr.UserDialogs;
using Rg.Plugins.Popup.Services;
using SI_Master.Models;
using SI_Master.PopUps;
using SI_Master.ViewModels.CardsPage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SI_Master.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CardsPage : ContentPage
    {

        ICardsPageViewModel _viewModel = DependencyService.Get<ICardsPageViewModel>();
        public CardsPage()
        {
            InitializeComponent();
            BindingContext = _viewModel;
            CardsListView.ItemSelected += (object sender, SelectedItemChangedEventArgs e) =>
            {
                var limits = (Card)e.SelectedItem;
                var cardLimitsPopup = new CardLimitsPopup(limits.CardLimits);
                PopupNavigation.Instance.PushAsync(cardLimitsPopup);
            };


        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            if(_viewModel.ItemSource == null)
            {
                UserDialogs.Instance.ShowLoading();
                await _viewModel.LoadCards();
                UserDialogs.Instance.HideLoading();
            }
        }
        public async void OnMore(object sender, EventArgs e)
        {
            var mi = (MenuItem)sender;
            var sc = (Card)mi.CommandParameter;
            DictionaryLimits dic = await _viewModel.GetLimitsFromProvider(sc.Supplier);
            _ = Navigation.PushAsync(new ChangeLimitsPage(dic));
        }

        public void OnDelete(object sender, EventArgs e)
        {
            var mi = ((MenuItem)sender);
            //DisplayAlert("Заблокировать выбранную карту?", mi.CommandParameter + "Отмена", "OK");
            UserDialogs.Instance.Alert("Заблокировать выбранную карту?", null, "Оk");
        }

        private void SearchBarTextChanged(object sender, TextChangedEventArgs e)
        {
            CardsListView.ItemsSource = _viewModel.ItemSource.FindAll(i => i.Caption.Contains(e.NewTextValue));
        }
    }
}