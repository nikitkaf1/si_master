﻿using Acr.UserDialogs;
using SI_Master.ViewModels.ArchiveCardOrderPage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SI_Master.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ArchiveCardOrderPage : ContentPage
    {
        IArchiveCardOrderPageViewModel viewModel = DependencyService.Get<IArchiveCardOrderPageViewModel>();
        public ArchiveCardOrderPage()
        {
            InitializeComponent();
            BindingContext = viewModel;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            UserDialogs.Instance.ShowLoading();
            await viewModel.LoadArchiveCardOrder();
            UserDialogs.Instance.HideLoading();
        }
    }
}