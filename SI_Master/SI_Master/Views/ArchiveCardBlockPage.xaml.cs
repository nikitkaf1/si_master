﻿using Acr.UserDialogs;
using SI_Master.ViewModels.ArchiveCardBlockPage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SI_Master.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ArchiveCardBlockPage : ContentPage
    {
        IArchiveCardBlockViewModel viewModel = DependencyService.Get<IArchiveCardBlockViewModel>();
        public ArchiveCardBlockPage()
        {
            InitializeComponent();
            BindingContext = viewModel;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            UserDialogs.Instance.ShowLoading();
            await viewModel.LoadArchiveCardBlock();
            UserDialogs.Instance.HideLoading();
        }
    }
}