﻿using Acr.UserDialogs;
using SI_Master.ViewModels.SettlementsPage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SI_Master.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SettlementsPage : ContentPage
    {

        ISerttlementsPageViewModel viewmodel = DependencyService.Get<ISerttlementsPageViewModel>();
        public SettlementsPage()
        {
            InitializeComponent();
            BindingContext = viewmodel;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            UserDialogs.Instance.ShowLoading();
            await viewmodel.LoadSettlements();
            UserDialogs.Instance.HideLoading();
        }
    }
}