﻿using SI_Master.Models;
using SI_Master.ViewModels.DeskTopPage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SI_Master.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DeskTopPage : ContentPage
    {
        IDeskTopPageViewModel _viewModel = DependencyService.Get<IDeskTopPageViewModel>();
        
        public DeskTopPage( )
        {
            InitializeComponent();
            BindingContext = _viewModel;
            
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            //((NavigationPage)Application.Current.MainPage).BarBackgroundColor = (Color)App.Current.Resources["toolBarBackColor"];
            //((NavigationPage)Application.Current.MainPage).BarTextColor = (Color)App.Current.Resources["toolBarTextColor"];

            UserDialogs.Instance.ShowLoading();
            await _viewModel.LoadDesktop();
            UserDialogs.Instance.HideLoading();
        }


        private async void Register(object sender, EventArgs e)
        {
            AuthData authData = await _viewModel.DeviceRegistration();
            await DisplayAlert("", "Client" + authData.Client + " " + "Token" + authData.Token, "Ok");
        }
    }

}