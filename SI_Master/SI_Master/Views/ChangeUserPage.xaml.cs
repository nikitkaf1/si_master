﻿using SI_Master.Common;
using SI_Master.Models;
using SI_Master.Settings;
using SI_Master.ViewModels.ChangeUserPage;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SI_Master.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChangeUserPage : ContentPage
    {

        private bool _tryLoginActive;

        private bool? _loginInput;

        private readonly IChangeUserPageViewModel _viewModel = DependencyService.Get<IChangeUserPageViewModel>();
        
        public ChangeUserPage()
        {
            InitializeComponent();
            BindingContext = _viewModel;
            //((NavigationPage)Application.Current.MainPage).BarBackgroundColor = (Color)App.Current.Resources["toolBarBackColor"];
            //((NavigationPage)Application.Current.MainPage).BarTextColor = (Color)App.Current.Resources["toolBarTextColor"];
        }

        private async void RemoveUser(UserAuthData user)
        {
            if (await DisplayAlert("", "Удалить учетную запись?", "Да", "Нет"))
            {
                if (usersListView.SelectedItem == user)
                {
                    usersListView.SelectedItem = null;
                    selectButton.IsVisible = false;
                }
                _viewModel.Users.Remove(user);
                AuthSettings.Remove(user.Login);
                if (_viewModel.Users.Count == 0) ShowLoginInput();
            }
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();

            int activeUser = AuthSettings.Active;
            if (_tryLoginActive && activeUser > -1 && activeUser < _viewModel.Users.Count)
            {
                _tryLoginActive = false;
                UserAuthData user = _viewModel.Users[activeUser];

                //Dictionary<string, object> request = new Dictionary<string, object>()
                //{
                //    { "key", "page__auth" },
                //    { "field__login", user.Login },
                //    { "field__password", user.Password }
                //};

                ShowBusy();

                //App.Rest.CurrentUser = user;
                bool r = await _viewModel.Login(user);
                if (r)
                {
                    Application.Current.MainPage = new NavigationPage(new MainPage())
                    {
                        BarBackgroundColor = (Color)App.Current.Resources["toolBarBackColor"],
                        BarTextColor = (Color)App.Current.Resources["toolBarTextColor"]

                    };
                    await Navigation.PopAsync();
                    return;
                }

                if (r) AuthSettings.Active = -1;

                ShowLoginInput();
                return;
            }

            if (_loginInput.HasValue)
            {
                if (_loginInput.Value) ShowLoginInput(); else ShowUserSelection();
            }
            else
            {
                if (_viewModel.Users.Count > 0) ShowUserSelection(); else ShowLoginInput();
            }
        }

        private void ShowBusy()
        {
            loginGroup.IsVisible = false;
            userSelectionGroup.IsVisible = false;
            busyIndicator.IsEnabled = true;
            busyIndicator.IsVisible = true;
            busyIndicator.IsRunning = true;
        }

        private void ShowInvalidCaptions(bool show)
        {
            loginInvalidCaption.IsVisible = show;
            passwordInvalidCaption.IsVisible = show;
            codeInvalidCaption.IsVisible = show;
        }

        private void ShowLoginInput()
        {
            busyIndicator.IsEnabled = false;
            busyIndicator.IsVisible = false;
            busyIndicator.IsRunning = false;
            userSelectionGroup.IsVisible = false;
            ShowInvalidCaptions(false);
            Title = "Вход";
            loginGroup.IsVisible = true;
            _loginInput = true;
        }

        private void ShowUserSelection()
        {
            DependencyService.Get<IKeyboardHelper>().HideKeyboard();
            busyIndicator.IsEnabled = false;
            busyIndicator.IsVisible = false;
            busyIndicator.IsRunning = false;
            loginGroup.IsVisible = false;
            Title = "Учетные записи";
            userSelectionGroup.IsVisible = true;
            UsersListView_ItemSelected(usersListView, null);
            _loginInput = false;
        }

        private async void LoginButton_Clicked(object sender, EventArgs e)
        {
            #region validate input fields

            if (string.IsNullOrWhiteSpace(loginEntry.Text)) // min length ???
            {
                ShowInvalidCaptions(true);
                loginEntry.Focus();
                return;
            }
            if (string.IsNullOrWhiteSpace(passwordEntry.Text)) // min length ???
            {
                ShowInvalidCaptions(true);
                passwordEntry.Focus();
                return;
            }
            if (string.IsNullOrWhiteSpace(codeEntry.Text))
            {
                ShowInvalidCaptions(true);
                codeEntry.Focus();
                return;
            }

            #endregion

            Dictionary<string, object> request = new Dictionary<string, object>()
            {
                { "key", "page__login" },
                { "field__login", loginEntry.Text },
                { "field__password", passwordEntry.Text },
                { "field__code", codeEntry.Text }
            };

            ShowBusy();

            UserAuthData user = new UserAuthData()
            {
                Caption = "Новый пользователь", // ??? get from response.Data
                Login = loginEntry.Text,
                Password = passwordEntry.Text
            };

            //App.Rest.CurrentUser = user;
            bool r = await _viewModel.Login(user);
            if (!r)
            {
                ShowLoginInput();
                await DisplayAlert("", "Ошибка сети", "OK");
                return;
            }

            AuthSettings.Active = AuthSettings.AddOrUpdate(user);
            Application.Current.MainPage = new NavigationPage(new MainPage())
            {
                BarBackgroundColor = (Color)App.Current.Resources["toolBarBackColor"],
                BarTextColor = (Color)App.Current.Resources["toolBarTextColor"]
            };

            await Navigation.PopAsync();
        }

        private async void SelectButton_Clicked(object sender, EventArgs e)
        {
            if (usersListView.SelectedItem == null) return;
            UserAuthData user = (UserAuthData)usersListView.SelectedItem;
            usersListView.SelectedItem = null;

            Dictionary<string, object> request = new Dictionary<string, object>()
            {
                { "key", "page__auth" },
                { "field__login", user.Login },
                { "field__password", user.Password }
            };

            ShowBusy();

            //App.Rest.CurrentUser = user;
            bool r = await _viewModel.Login(user);
            if (!r)
            {
                ShowUserSelection();
                await DisplayAlert("", "Ошибка сети", "OK");
                return;
            }

            AuthSettings.Active = _viewModel.Users.IndexOf(user);

            Application.Current.MainPage = new NavigationPage(new MainPage())
            {
                BarBackgroundColor = (Color)App.Current.Resources["toolBarBackColor"],
                BarTextColor = (Color)App.Current.Resources["toolBarTextColor"]
            };

            await Navigation.PopAsync();
        }

        private void LoginHelp_Tapped(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://soft-enginiring.ru/forget-password/"));
        }

        private void OtherAccounts_Tapped(object sender, EventArgs e)
        {
            ShowUserSelection();
        }

        private void NewAccount_Tapped(object sender, EventArgs e)
        {
            ShowLoginInput();
        }

        private void PhoneLabel_Tapped(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("tel:88003015715"));
        }

        private void PrivacyPolicyLabel_Tapped(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://soft-enginiring.ru/confidential.pdf"));
        }

        private void UsersListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            selectButton.IsVisible = (usersListView.SelectedItem != null);
        }
    }
}