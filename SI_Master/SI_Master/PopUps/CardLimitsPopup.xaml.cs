﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using SI_Master.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SI_Master.PopUps
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CardLimitsPopup : PopupPage
    {

        private List<Limits> _DataSource;
        public List<Limits> DataSource
        {
            get
            {
                return _DataSource;
            }
            set
            {
                _DataSource = value;
                OnPropertyChanged("DataSource");
            }
        }
        public CardLimitsPopup(List<Limits> limits)
        {
            InitializeComponent();
            BindingContext = this;
            Title = "Лимиты карты";
            DataSource = limits;
            if (limits == null | limits.Count == 0)
            {
                LimitsList.IsVisible = false;
                PlaceHolderLabel.IsVisible = true;
            } 
        }

        private void Ok_Clicked(object sender, EventArgs e)
        {
            PopupNavigation.Instance.PopAsync();
        }
    }
}