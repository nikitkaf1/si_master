﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SI_Master.Managers.ArchiveCardLimitsManager;
using SI_Master.Models;
using SI_Master.REST;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

[assembly: Dependency(typeof(ArchiveCardLimitsManager))]
namespace SI_Master.Managers.ArchiveCardLimitsManager
{
    public class ArchiveCardLimitsManager : IArchiveCardLimitsManager
    {

        IAuthManager authmanager = DependencyService.Get<IAuthManager>();
        INetworkService networkservice = DependencyService.Get<INetworkService>();
        public async Task<List<ArchiveCardLimits>> GetArchiveCardLimits()
        {
            Answer answer = new Answer();
            List<ArchiveCardLimits> ArchiveCardLimitsList = new List<ArchiveCardLimits>();
            try
            {
                answer = await networkservice.NetworkRequest(NetworkService.TaskType.ArchiveLimits, authmanager.GetAuthData());
                if (answer != null && answer.ResData is JObject jData)
                {
                    if (jData.TryGetValue("items", out JToken jArray))
                    {
                        ArchiveCardLimitsList = JsonConvert.DeserializeObject<List<ArchiveCardLimits>>(jArray.ToString());
                    }
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
            }
            return ArchiveCardLimitsList;
        }
    }
}
