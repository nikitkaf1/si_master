﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SI_Master.Managers.TransactionsManager;
using SI_Master.Models;
using SI_Master.REST;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

[assembly: Dependency(typeof(TransactionsManager))]
namespace SI_Master.Managers.TransactionsManager
{
    public class TransactionsManager : ITransactionsManager
    {
        IAuthManager authmanager = DependencyService.Get<IAuthManager>();
        INetworkService networkservice = DependencyService.Get<INetworkService>();


        public async Task<List<Transaction>> Gettransactions()
        {
            Answer answer = new Answer();
            List<Transaction> TransactionsList = new List<Transaction>();
            try
            {
                answer = await networkservice.NetworkRequest(NetworkService.TaskType.Transactions, authmanager.GetAuthData());
                if (answer != null && answer.ResData is JObject jData)
                {
                    if (jData.TryGetValue("items", out JToken jArray))
                    {
                        TransactionsList = JsonConvert.DeserializeObject<List<Transaction>>(jArray.ToString());
                    }
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
            }
            return TransactionsList;
        }
    }
}
