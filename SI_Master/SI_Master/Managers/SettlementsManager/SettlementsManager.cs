﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SI_Master.Managers.SettlementsManager;
using SI_Master.Models;
using SI_Master.REST;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

[assembly: Dependency(typeof(SettlementsManager))]
namespace SI_Master.Managers.SettlementsManager
{
    public class SettlementsManager: ISettlementsManager
    {

        IAuthManager authmanager = DependencyService.Get<IAuthManager>();
        INetworkService networkservice = DependencyService.Get<INetworkService>();
        public async Task<List<Settlement>> GetSettlements()
        {
            Answer answer = new Answer();
            List<Settlement> SettlementsList = new List<Settlement>();
            try
            {
                answer = await networkservice.NetworkRequest(NetworkService.TaskType.Settlements, authmanager.GetAuthData());
                if (answer != null && answer.ResData is JObject jData)
                {
                    if (jData.TryGetValue("items", out JToken jArray))
                    {
                        SettlementsList = JsonConvert.DeserializeObject<List<Settlement>>(jArray.ToString());
                    }
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
            }
            return SettlementsList;
        }
    }
}
