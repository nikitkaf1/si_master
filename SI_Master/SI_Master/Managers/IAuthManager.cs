﻿using SI_Master.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SI_Master.Managers
{
    public interface IAuthManager
    {
        Task<Answer> Login(UserAuthData user);
        Task<AuthData> RegisterDevice(string login, string phoneNumber);

        AuthData GetAuthData();
    }
}
