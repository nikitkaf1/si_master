﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SI_Master.Managers.ArchiveCardOrderManager;
using SI_Master.Models;
using SI_Master.REST;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

[assembly: Dependency(typeof(ArchiveCradOrderManager))]
namespace SI_Master.Managers.ArchiveCardOrderManager
{
    public class ArchiveCradOrderManager : IArchiveCradOrderManager
    {

        IAuthManager authmanager = DependencyService.Get<IAuthManager>();
        INetworkService networkservice = DependencyService.Get<INetworkService>();
        public async Task<List<CardOrder>> GetArchiveCardOrders()
        {
            Answer answer = new Answer();
            List<CardOrder> ArchiveCardOrderList = new List<CardOrder>();
            try
            {
                answer = await networkservice.NetworkRequest(NetworkService.TaskType.ArchiveCards, authmanager.GetAuthData());
                if (answer != null && answer.ResData is JObject jData)
                {
                    if (jData.TryGetValue("items", out JToken jArray))
                    {
                        ArchiveCardOrderList = JsonConvert.DeserializeObject<List<CardOrder>>(jArray.ToString());
                    }
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
            }
            return ArchiveCardOrderList;
        }
    }
}
