﻿using SI_Master.Common;
using SI_Master.Managers;
using SI_Master.Models;
using SI_Master.REST;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

[assembly: Dependency(typeof(AuthManager))]
namespace SI_Master.Managers
{
    public class AuthManager : IAuthManager
    {
        INetworkService netwrokSevice = DependencyService.Get<INetworkService>();
        IDeviceInfo deviceInfo = DependencyService.Get<IDeviceInfo>();
        public async Task<Answer> Login(UserAuthData user)
        {
            object[] args = new object[1];
            args[1] = user;
            Answer answer = await netwrokSevice.NetworkRequest(NetworkService.TaskType.Login, null , user);
            return answer;
        }

        public async Task<AuthData> RegisterDevice(string login, string phoneNumber)
        {
            DeviceRegistration registrationData = new DeviceRegistration(){
                //MOK
                Login = login,
                Shared = "MltOzaOAUlSpDwJ3IgYMVOukOD18vO9gefodGe5i57M=",
                SharedId = "8",
                DeviceId = deviceInfo.GetIdentifier(),
                DeviceModel = "A6010",
                PhoneNumber = phoneNumber,
            };
            var authData = await netwrokSevice.Login(registrationData);

            return authData;
        }

        public AuthData GetAuthData()
        {
            AuthData authData = new AuthData()
            {
                Client = "329BD8",
                Token = "w+XZZ0xkFXGo3ojUJ2DciPxeWMuX45mPe9s7vV9tZHM=",
                Shared = "MltOzaOAUlSpDwJ3IgYMVOukOD18vO9gefodGe5i57M=",
                SharedId = "8",
                DeviceId = "35744307a4c44e6d",
                //DeviceId = deviceInfo.GetIdentifier()
            };
            return authData;
        }
    }
}
