﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SI_Master.Managers.ArchiveCardBlockManager;
using SI_Master.Models;
using SI_Master.REST;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

[assembly: Dependency(typeof(ArchiveCardBlockManager))]
namespace SI_Master.Managers.ArchiveCardBlockManager
{
    public class ArchiveCardBlockManager: IArchiveCardBlockManager
    {
        IAuthManager authmanager = DependencyService.Get<IAuthManager>();
        INetworkService networkservice = DependencyService.Get<INetworkService>();

        public async Task<List<Block>> GetArchiveCardLimits()
        {
            Answer answer = new Answer();
            List<Block> ArchiveCardLimitsBlock = new List<Block>();
            try
            {
                answer = await networkservice.NetworkRequest(NetworkService.TaskType.ARchiveCardBlock, authmanager.GetAuthData());
                if (answer != null && answer.ResData is JObject jData)
                {
                    if (jData.TryGetValue("items", out JToken jArray))
                    {
                        ArchiveCardLimitsBlock = JsonConvert.DeserializeObject<List<Block>>(jArray.ToString());
                    }
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
            }
            return ArchiveCardLimitsBlock;
        }
    }
}
