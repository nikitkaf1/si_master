﻿using SI_Master.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SI_Master.REST
{
    public interface INetworkService
    {
        Task<Answer> NetworkRequest(NetworkService.TaskType type, AuthData authData, params object[] args);
        Task<AuthData> Login(DeviceRegistration registrationData);
    }
}
