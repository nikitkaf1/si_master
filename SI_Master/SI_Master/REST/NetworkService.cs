﻿using SI_Master.Models;
using SI_Master.REST;
using System.Text;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Refit;
using Xamarin.Forms;
using System.Linq;
using System.IO;


[assembly: Dependency(typeof(NetworkService))]
namespace SI_Master.REST
{
    public class NetworkService : INetworkService
    {

        private const string baseUrl = "https://mobilebakend.soft-enginiring.ru";

        CookieContainer cookieContainer;
        private Api api;

        HttpClient client;
        private bool Initialized = false;

        public enum TaskType
        {
            Login,
            Getdashboard,
            OrderKeys,
            MobileAppCode,
            GetDashBoardMisc,
            Cards,
            CardLimits,
            CardOrder,
            LockAndUbnlockCards,
            SetCardHolder,
            Transactions,
            Settlements,
            Documents,
            DocumentsBill,
            DocumentsDownload,
            DocumentsSI,
            ARchiveCardBlock,
            ArchiveLimits,
            ArchiveCards,
            Contracts,
            PromisedPayment,
            PromesidPaymentAplly,
            DictionaryLimits
        }

        private void Init(string Url)
        {

            cookieContainer = new CookieContainer();
            var handler = new LoggingHandler(new HttpClientHandler()
            {
                CookieContainer = cookieContainer,
                UseCookies = true
            });
            client = new HttpClient(handler)
            {
                BaseAddress = new Uri(Url),
                Timeout = new TimeSpan(0, 0, 120)
            };
            api = RestService.For<Api>(client);

            Initialized = true;
        }


        public async Task<Answer> NetworkRequest(TaskType type, AuthData authData,  params object[] args)
        {
            if(!Initialized)
            {
                Init(baseUrl);
            }
            Answer answer = null;
            switch (type)
            {
                //case TaskType.Login:
                //    //TODO Login
                //    break;
                case TaskType.Getdashboard:
                    answer = await api.Getdashboard(authData);
                    break;
                case TaskType.Cards:
                    answer = await api.Cards(null, authData);
                    break;
                case TaskType.Transactions:
                    answer = await api.Transactions(null, authData);
                    break;
                case TaskType.Settlements:
                    answer = await api.Settlements(null, authData);
                    break;
                case TaskType.ArchiveLimits:
                    answer = await api.ArchiveLimits(null, authData);
                    break;
                case TaskType.ARchiveCardBlock:
                    answer = await api.ARchiveCardBlock(null, authData);
                    break;
                case TaskType.ArchiveCards:
                    answer = await api.ArchiveCards(null, authData);
                    break;
                case TaskType.DictionaryLimits:
                    answer = await api.DictionaryLimits(authData);
                    break;
            }

            return answer;
        }

        public async Task<AuthData> Login(DeviceRegistration registrationData)
        {
            if (!Initialized)
            {
                Init(baseUrl);
            }
            AuthData authData = new AuthData();
            authData = await api.Login(registrationData);
            return authData;
        }
    }
}
