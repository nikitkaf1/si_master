﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SI_Master.Models
{
    public class LimitsItem
    {
        public string LimitsName { get; set; }
        public List<string> FuelTypesList { get; set; }
        public List<string> LimitTypes { get; set; }
        public List<string> Country { get; set; }
        public List<string> RefuilimgStations { get; set; }
    }
}
