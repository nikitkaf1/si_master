﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SI_Master.Models
{
    public enum MenuItemType
    {
        DeskTop,
        Settlements,
        Cards,
        Transactions,
        ArchiveCardLimits,
        ArchiveCardOrder,
        ArchiveCardBlock,
        Dashboard,
        ChangeUser,
        Exit
    }
    public class HomeMenuItem
    {
        public MenuItemType Id { get; set; }

        public string Title { get; set; }
    }
}
