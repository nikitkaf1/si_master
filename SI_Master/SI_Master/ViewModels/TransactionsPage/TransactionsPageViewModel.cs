﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using SI_Master.Managers.TransactionsManager;
using SI_Master.Models;
using SI_Master.ViewModels.TransactionsPage;

[assembly: Dependency(typeof(TransactionsPageViewModel))]
namespace SI_Master.ViewModels.TransactionsPage
{
    public class TransactionsPageViewModel : BaseViewModel, ITransactionsPageViewModel
    {

        ITransactionsManager transactionsManager = DependencyService.Get<ITransactionsManager>();

        public string Title { get; set; } = "Транзакции";
        private List<Transaction> _itemsource;
        public List<Transaction> ItemSource
        { get {
                return _itemsource;
            } set {
                _itemsource = value;
                OnPropertyChanged("ItemSource");
            }
             }

        public async Task LoadTransactions()
        {
            ItemSource = await transactionsManager.Gettransactions();
        }
    }
}
