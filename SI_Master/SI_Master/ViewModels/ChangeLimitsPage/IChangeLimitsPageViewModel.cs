﻿using SI_Master.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SI_Master.ViewModels.ChangeLimitsPage
{
    public interface IChangeLimitsPageViewModel
    {
        void SetDictionary(DictionaryLimits dictionaryLimits);
        string Title { get; set; }

        List<LimitsItem> DataSource { get; set; }

    }
}
