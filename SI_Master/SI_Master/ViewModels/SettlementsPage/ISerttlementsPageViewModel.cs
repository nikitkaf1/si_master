﻿using SI_Master.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SI_Master.ViewModels.SettlementsPage
{
    public interface ISerttlementsPageViewModel
    {
        string Title { get; set; }
        List<Settlement> ItemSource { get; set; }
        Task LoadSettlements();
    }
}
