﻿using SI_Master.Managers.SettlementsManager;
using SI_Master.Models;
using SI_Master.ViewModels.SettlementsPage;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

[assembly: Dependency(typeof(SerttlementsPageViewModel))]
namespace SI_Master.ViewModels.SettlementsPage
{
    public class SerttlementsPageViewModel : BaseViewModel, ISerttlementsPageViewModel
    {

        ISettlementsManager settlementsmanager = DependencyService.Get<ISettlementsManager>();
        public string Title { get; set; } = "Взаиморасчёты";
        private List<Settlement> _itemsource { get; set; }
        public List<Settlement> ItemSource
        { get {
                return _itemsource;
            } set {
                _itemsource = value;
                OnPropertyChanged("ItemSource");
            }
        }

        public async Task LoadSettlements()
        {
            ItemSource = await settlementsmanager.GetSettlements();
        }
    }
}
