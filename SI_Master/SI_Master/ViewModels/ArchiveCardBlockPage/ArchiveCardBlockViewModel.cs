﻿using SI_Master.Managers.ArchiveCardBlockManager;
using SI_Master.Models;
using SI_Master.ViewModels.ArchiveCardBlockPage;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

[assembly: Dependency(typeof(ArchiveCardBlockViewModel))]
namespace SI_Master.ViewModels.ArchiveCardBlockPage
{
    public class ArchiveCardBlockViewModel : BaseViewModel, IArchiveCardBlockViewModel
    {

        IArchiveCardBlockManager archivCardBlockManager = DependencyService.Get<IArchiveCardBlockManager>();
        public string Title { get; set; } = "Заявки на блокировку";

        private List<Block> _itemsource { get; set; }
        public List<Block> ItemSource { get {
                return _itemsource;
            } set {
                _itemsource = value;
                OnPropertyChanged("ItemSource");
            } }

        public async Task LoadArchiveCardBlock()
        {
            ItemSource = await archivCardBlockManager.GetArchiveCardLimits();
        }
    }
}
