﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using System.Threading.Tasks;
using SI_Master.Managers;
using SI_Master.Models;
using SI_Master.ViewModels.DashboardPage;
using Xamarin.Forms;

[assembly: Dependency(typeof(DashboardPageViewModel))]
namespace SI_Master.ViewModels.DashboardPage
{
    public class DashboardPageViewModel : BaseViewModel, IDashboardPageViewModel
    {
        public string Title { get; set; }  = "Карточка предприятия";
        IDashboardManager dashboardManager = DependencyService.Get<IDashboardManager>();

        private AboutOrganozation _Dashboard = new AboutOrganozation();
        public AboutOrganozation Dashboard { get {
                return _Dashboard;
            } set {
                _Dashboard = value;
                OnPropertyChanged("Dashboard");
            } }

        public async Task<AboutOrganozation> Dachboard()
        {
            Dashboard = await dashboardManager.GetDashboard();
            return Dashboard;
        }
        public async Task LoadDashboard()
        {
            Dashboard = await dashboardManager.GetDashboard();
        }
    }
}

