﻿using SI_Master.Managers;
using SI_Master.Models;
using SI_Master.ViewModels.DeskTopPage;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

[assembly: Dependency(typeof(DeskTopPageViewModel))]
namespace SI_Master.ViewModels.DeskTopPage
{
    public class DeskTopPageViewModel : BaseViewModel, IDeskTopPageViewModel
    {

        IAuthManager authManager = DependencyService.Get<IAuthManager>();
        IDashboardManager dashboardManager = DependencyService.Get<IDashboardManager>();
        public string Title { get; set; } = "Рабочий стол";

        private List<Contracts> _ItemSource;
        public List<Contracts> ItemSource { get {
                return _ItemSource;
            } set {
                _ItemSource = value;
                OnPropertyChanged("ItemSource");
            } }


        public DeskTopPageViewModel()
        {
            //MOK
            //ItemSource = new ObservableCollection<Contract>();
            //ItemSource.Add(new Contract
            //{
            //    Block = false,
            //    Provider = "Белкамнефть",
            //    Number = "666",
            //    Date = DateTime.Now,
            //});
            //ItemSource.Add(new Contract
            //{
            //    Block = true,
            //    Provider = "Волкамводку",
            //    Number = "777",
            //    Date = DateTime.Now,
            //});
        }

        public async Task<AuthData> DeviceRegistration()
        {
            AuthData authData = await authManager.RegisterDevice("Ц00000820", "+79501571579");
            return authData;
        }

        public async Task LoadDesktop()
        {
            ItemSource = await dashboardManager.GetDesktop();
        }
    }
}
