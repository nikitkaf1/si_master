﻿using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;

namespace SI_Master.Droid
{
    [Activity(Label = "СОФТИНЖИНИРИНГ", Icon = "@drawable/logo_round", Theme = "@style/SplashScreen", MainLauncher = true, NoHistory = true,
        ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class SplashActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.SplashScreen);
            TextView tv = FindViewById<TextView>(Resource.Id.txtAppVersion);
            tv.Text = @"Версия " + PackageManager.GetPackageInfo(PackageName, 0).VersionName;
        }

        protected override void OnResume()
        {
            base.OnResume();
            Task task = new Task(() => { StartMainActivity(); });
            task.Start();
        }

        public override void OnBackPressed() { }

        async void StartMainActivity()
        {
            await Task.Delay(500); 
            StartActivity(new Intent(Application.Context, typeof(MainActivity)));
        }
    }
}